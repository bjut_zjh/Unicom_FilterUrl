package com.unicom.process;

import backtype.storm.Config;
import backtype.storm.LocalCluster;
import backtype.storm.StormSubmitter;
import backtype.storm.generated.AlreadyAliveException;
import backtype.storm.generated.InvalidTopologyException;
import backtype.storm.generated.StormTopology;
import backtype.storm.spout.SpoutOutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.TopologyBuilder;
import backtype.storm.topology.base.BaseRichSpout;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Values;
import backtype.storm.utils.Utils;
import com.unicom.utils.DateUtil;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.storm.hdfs.bolt.HdfsBolt;
import org.apache.storm.hdfs.bolt.format.DefaultFileNameFormat;
import org.apache.storm.hdfs.bolt.format.DelimitedRecordFormat;
import org.apache.storm.hdfs.bolt.format.FileNameFormat;
import org.apache.storm.hdfs.bolt.format.RecordFormat;
import org.apache.storm.hdfs.bolt.rotation.FileRotationPolicy;
import org.apache.storm.hdfs.bolt.rotation.FileSizeRotationPolicy;
import org.apache.storm.hdfs.bolt.rotation.TimedRotationPolicy;
import org.apache.storm.hdfs.bolt.sync.CountSyncPolicy;
import org.apache.storm.hdfs.bolt.sync.SyncPolicy;
import storm.kafka.BrokerHosts;
import storm.kafka.KafkaSpout;
import storm.kafka.SpoutConfig;
import storm.kafka.ZkHosts;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import java.util.Random;
import java.util.UUID;

public class StormToHDFSProcess {
    public static void main(String[] args) throws AlreadyAliveException, InvalidTopologyException, InterruptedException {
        TopologyBuilder builder = new TopologyBuilder();
        BrokerHosts hosts = new ZkHosts("192.168.30.133:2181");
        String topic = "myFtp";
        String id = UUID.randomUUID().toString();
        String zkRoot = "/kafkaSpout";
        SpoutConfig spoutConf = new SpoutConfig(hosts, topic, zkRoot, id);

        String kafkaSpout = KafkaSpout.class.getSimpleName();
//		String logBolt = UrlFilterBolt.class.getSimpleName();

//        builder.setSpout(kafkaSpout, new KafkaSpout(spoutConf));


        // use "|" instead of "," for field delimiter
        RecordFormat format = new DelimitedRecordFormat()
                .withFieldDelimiter(" : ");

        // sync the filesystem after every 1k tuples
        SyncPolicy syncPolicy = new CountSyncPolicy(1000);
        //FileRotationPolicy rotationPolicy = new FileSizeRotationPolicy(5.0f, FileSizeRotationPolicy.Units.MB);

        // rotate files
        FileRotationPolicy rotationPolicy = new TimedRotationPolicy(1.0f, TimedRotationPolicy.TimeUnit.MINUTES);

        FileNameFormat fileNameFormat = new DefaultFileNameFormat()
                .withPath("/storm/" + DateUtil.getNowStr() + "/").withPrefix("app_").withExtension(".log");

        HdfsBolt hdfsBolt = new HdfsBolt()
                .withFsUrl("hdfs://192.168.1.104:9000")
                .withFileNameFormat(fileNameFormat)
                .withRecordFormat(format)
                .withRotationPolicy(rotationPolicy)
                .withSyncPolicy(syncPolicy);

        builder.setSpout(kafkaSpout, new KafkaSpout(spoutConf), 3);
        builder.setBolt("hdfs-bolt", hdfsBolt, 2).fieldsGrouping(kafkaSpout, new Fields("bytes"));

        Config conf = new Config();

        String name = StormToHDFSProcess.class.getSimpleName();
//        if (args != null && args.length > 0) {
//            conf.put(Config.NIMBUS_HOST, args[0]);
//            conf.setNumWorkers(3);
//            StormSubmitter.submitTopologyWithProgressBar(name, conf, builder.createTopology());
//        } else {
//            conf.setMaxTaskParallelism(3);
//            LocalCluster cluster = new LocalCluster();
//            cluster.submitTopology(name, conf, builder.createTopology());
//            Thread.sleep(60000);
//            cluster.shutdown();
//        }

//        LocalCluster localCluster = new LocalCluster();
//        StormTopology createTopology = builder.createTopology();
//        localCluster.submitTopology(name, conf, createTopology);

        try {
            Config stormConf = new Config();
            StormSubmitter.submitTopology("StormTopology", stormConf, builder.createTopology());
        } catch (AlreadyAliveException e) {
            e.printStackTrace();
        } catch (InvalidTopologyException e) {
            e.printStackTrace();
        }
    }
}
