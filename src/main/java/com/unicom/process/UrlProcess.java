package com.unicom.process;

import java.util.UUID;

import storm.kafka.BrokerHosts;
import storm.kafka.KafkaSpout;
import storm.kafka.SpoutConfig;
import storm.kafka.ZkHosts;
import backtype.storm.Config;
import backtype.storm.LocalCluster;
import backtype.storm.generated.StormTopology;
import backtype.storm.topology.TopologyBuilder;

public class UrlProcess {
	public static void main(String[] args) {
		TopologyBuilder topologyBuilder = new TopologyBuilder();
		BrokerHosts hosts = new ZkHosts("192.168.1.103:2181");
		String topic = "myFtp";
		String id = UUID.randomUUID().toString();
		String zkRoot = "/kafkaSpout";
		SpoutConfig spoutConf = new SpoutConfig(hosts, topic, zkRoot, id);

		String kafkaSpout = KafkaSpout.class.getSimpleName();
//		String logBolt = UrlFilterBolt.class.getSimpleName();

		topologyBuilder.setSpout(kafkaSpout, new KafkaSpout(spoutConf));
//		topologyBuilder.setBolt(logBolt, new UrlFilterBolt()).shuffleGrouping(
//				kafkaSpout);

		LocalCluster localCluster = new LocalCluster();
		String simpleName = UrlProcess.class.getSimpleName();
		StormTopology createTopology = topologyBuilder.createTopology();
		Config config = new Config();
		localCluster.submitTopology(simpleName, config, createTopology);
	}
}
