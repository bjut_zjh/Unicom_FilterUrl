package com.unicom.storm;

import java.util.UUID;

import backtype.storm.StormSubmitter;
import backtype.storm.generated.AlreadyAliveException;
import backtype.storm.generated.InvalidTopologyException;
import backtype.storm.tuple.Fields;
import com.unicom.utils.ConfigUtil;
import com.unicom.utils.DateUtil;
import org.apache.storm.hdfs.bolt.HdfsBolt;
import org.apache.storm.hdfs.bolt.format.DefaultFileNameFormat;
import org.apache.storm.hdfs.bolt.format.DelimitedRecordFormat;
import org.apache.storm.hdfs.bolt.format.FileNameFormat;
import org.apache.storm.hdfs.bolt.format.RecordFormat;
import org.apache.storm.hdfs.bolt.rotation.FileRotationPolicy;
import org.apache.storm.hdfs.bolt.rotation.TimedRotationPolicy;
import org.apache.storm.hdfs.bolt.sync.CountSyncPolicy;
import org.apache.storm.hdfs.bolt.sync.SyncPolicy;
import storm.kafka.BrokerHosts;
import storm.kafka.KafkaSpout;
import storm.kafka.SpoutConfig;
import storm.kafka.ZkHosts;
import backtype.storm.Config;
import backtype.storm.LocalCluster;
import backtype.storm.generated.StormTopology;
import backtype.storm.topology.TopologyBuilder;

public class StormToHdfsProcess {
	public static void main(String[] args) {
		TopologyBuilder topologyBuilder = new TopologyBuilder();
		BrokerHosts hosts = new ZkHosts(ConfigUtil.getString("datacenter.unicom.zookper"));
		String topic = ConfigUtil.getString("datacenter.unicom.kafkatopic");
		String id = UUID.randomUUID().toString();
		String zkRoot = ConfigUtil.getString("datacenter.unicom.zkRoot");
		SpoutConfig spoutConf = new SpoutConfig(hosts, topic, zkRoot, id);

		String kafkaSpout = KafkaSpout.class.getSimpleName();
		String urlFilterBolt = UrlFilterBolt.class.getSimpleName();

		// use "|" instead of "," for field delimiter
		RecordFormat format = new DelimitedRecordFormat()
				.withFieldDelimiter(" : ");

		// sync the filesystem after every 1k tuples
		SyncPolicy syncPolicy = new CountSyncPolicy(1000);
		//FileRotationPolicy rotationPolicy = new FileSizeRotationPolicy(5.0f, FileSizeRotationPolicy.Units.MB);

		// rotate files
		FileRotationPolicy rotationPolicy = new TimedRotationPolicy(1.0f, TimedRotationPolicy.TimeUnit.MINUTES);

		FileNameFormat fileNameFormat = new DefaultFileNameFormat()
				.withPath("/storm/011").withPrefix("app_").withExtension(".log");

		HdfsBolt hdfsBolt = new HdfsBolt()
				.withFsUrl(ConfigUtil.getString("datacenter.unicom.hdfs"))
				.withFileNameFormat(fileNameFormat)
				.withRecordFormat(format)
				.withRotationPolicy(rotationPolicy)
				.withSyncPolicy(syncPolicy);

		topologyBuilder.setSpout(kafkaSpout, new KafkaSpout(spoutConf));
		topologyBuilder.setBolt(urlFilterBolt, new UrlFilterBolt()).shuffleGrouping(
				kafkaSpout);
		topologyBuilder.setBolt("hdfs-bolt", hdfsBolt, 2).fieldsGrouping(urlFilterBolt, new Fields("url"));

		//集群发布配置
//		LocalCluster localCluster = new LocalCluster();
//		String simpleName = StormToHdfsProcess.class.getSimpleName();
//		StormTopology createTopology = topologyBuilder.createTopology();
//		Config config = new Config();
//		localCluster.submitTopology(simpleName, config, createTopology);

		try {
			Config stormConf = new Config();
			StormSubmitter.submitTopology("StormTopology", stormConf, topologyBuilder.createTopology());
		} catch (AlreadyAliveException e) {
			e.printStackTrace();
		} catch (InvalidTopologyException e) {
			e.printStackTrace();
		}
	}
}
