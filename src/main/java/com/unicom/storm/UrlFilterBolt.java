package com.unicom.storm;

import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import backtype.storm.task.OutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseRichBolt;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Tuple;
import backtype.storm.tuple.Values;

/**
 * storm的url的过滤bolt，去除不符合要求的url
 */
public class UrlFilterBolt extends BaseRichBolt {
    private OutputCollector collector;


    public void prepare(Map map, TopologyContext context,
                        OutputCollector collector) {
        this.collector = collector;
    }

    public void execute(Tuple input) {
        try {
            String str = new String(input.getBinaryByField("bytes"));

            String[] split = str.split("\\|");

            if (split.length == 15) {
                String url = split[13];
                if (!url.equals("")) {
                    //正则提取网址
                    String regexUrl = getRegexUrl(url);
                    split[13] = regexUrl;
                    str = str.replace(url, regexUrl);
                    this.collector.emit(input, new Values(str));
                    this.collector.ack(input);
                }
            }
        } catch (Exception e) {
            this.collector.fail(input);
            e.printStackTrace();
        }
    }

    private String getRegexUrl(String url) {
        Pattern pattern = Pattern
                .compile("(http://|ftp://|https://|www|){0,1}[^\u4e00-\u9fa5\\s]*?\\.(com|net|cn|me|tw|fr)");
        // 空格结束
        Matcher matcher = pattern
                .matcher(url);
        if (matcher.find()) {
            return matcher.group(0);
        }
        return url;
    }


    public void declareOutputFields(OutputFieldsDeclarer declarer) {
        declarer.declare(new Fields("url"));
    }

}
