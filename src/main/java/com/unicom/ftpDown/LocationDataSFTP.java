package com.unicom.ftpDown;

import com.google.gson.Gson;
import com.jcraft.jsch.ChannelSftp;
import com.unicom.utils.ConfigUtil;
import com.unicom.utils.DateUtil;
import com.unicom.utils.GlobalQueue;
import com.unicom.utils.SftpUtil;
import kafka.javaapi.producer.Producer;
import kafka.producer.KeyedMessage;
import kafka.producer.ProducerConfig;
import kafka.serializer.StringEncoder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Properties;
import java.util.zip.GZIPInputStream;

/**
 * 主类连接ftp下载数据并发送kafka
 * Created by bjut_zjh on 2016/3/22.
 */
public class LocationDataSFTP {
    private static Logger logger = LoggerFactory.getLogger(LocationDataSFTP.class);
    private static SftpUtil sftpUtil;
    private static Gson gson = new Gson();

    public static void main(String[] args) {
        try {
            //读取配置项ftp及kafka
            String host = ConfigUtil.getString("datacenter.unicom.ftphost");
            String port = ConfigUtil.getString("datacenter.unicom.port");
            String user = ConfigUtil.getString("datacenter.unicom.ftpuser");
            String password = ConfigUtil.getString("datacenter.unicom.ftppwd");
            String ftppath = ConfigUtil.getString("datacenter.unicom.ftppath");
            String province = ConfigUtil.getString("datacenter.unicom.province");
            String localpath = ConfigUtil.getString("datacenter.unicom.localpath");

            //获取kafka生产者对象
            Producer kafkaProducer = getKafkaProducer();

            sftpUtil = new SftpUtil();

            while (true) {
                ChannelSftp sftp = sftpUtil.connect(host, Integer.parseInt(port), user, password);
                //读取ftp路径的文件路径加入队列
//                sftpUtil.readPathToQueue(ftppath + "/" + DateUtil.getNowStr() + province, sftp);
                sftpUtil.readPathToQueue(ftppath, sftp);
                //获取ftp连接下载文件到本地路径
                String ftpFilePath = null;
                while (!GlobalQueue.arrayBlockingQueue.isEmpty()) {
                    //从队列中获取json格式的文件信息
                    HashMap<String, String> strMap = gson.fromJson(GlobalQueue.arrayBlockingQueue.poll().toString(), HashMap.class);
                    ftpFilePath = strMap.get("fileName");
                    if (ftpFilePath != null) {
                        //下载文件到本地并发送kafka，同時刪除ftp上的数据
                        sendToKafka(ftpFilePath, strMap.get("fileSize"), sftp, localpath + "/" + DateUtil.getNowStr(), kafkaProducer);
//                    sendToKafka(ftpFilePath, sftp, localpath + "/" +"20151115", kafkaProducer);
                        Thread.sleep(1000);
                    }
                }
//            Thread.sleep(5000);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static Producer getKafkaProducer() {
        Properties properties = new Properties();
        properties.put("zookeeper.connect", ConfigUtil.getString("datacenter.unicom.zookper"));//声明zk
        properties.put("serializer.class", StringEncoder.class.getName());
        properties.put("metadata.broker.list", ConfigUtil.getString("datacenter.unicom.kafkahost"));// 声明kafka broker
        return new Producer<String, String>(new ProducerConfig(properties));
    }

    /**
     * 下载文件到本地并发送kafka，同時刪除ftp上的数据
     *
     * @param ftpFilePath
     * @param sftp
     * @param localpath
     */
    private static void sendToKafka(String ftpFilePath, String fileSize, ChannelSftp sftp, String localpath, Producer kafkaProducer) {
        try {
            //获取日期下的文件夹目录
            String nowFilePath = ftpFilePath.split(DateUtil.getNowStr())[1];
            String localFilePath = localpath + nowFilePath.substring(0, nowFilePath.lastIndexOf("/"));
            File localFile = new File(localFilePath);
            if (!localFile.exists()) {
                localFile.mkdirs();
            }

            //ftp上文件名
            String gzName = ftpFilePath.substring(ftpFilePath.lastIndexOf("/") + 1);
            sftpUtil.download(ftpFilePath.substring(0, ftpFilePath.lastIndexOf("/")), gzName, fileSize, localFilePath + "/" + gzName, sftp);
            //下载文件到本地后删除远程ftp文件
            sftpUtil.delete(ftpFilePath.substring(0, ftpFilePath.lastIndexOf("/")), gzName, sftp);
            //读取文件发送到kafka
            BufferedReader reader = new BufferedReader(new InputStreamReader(new GZIPInputStream(new FileInputStream(localFilePath + "/" + gzName)), "UTF-8"));
            String line = null;
            while ((line = reader.readLine()) != null) {
                KeyedMessage<String, String> km = new KeyedMessage<String, String>(ConfigUtil.getString("datacenter.unicom.kafkatopic"), line);
                kafkaProducer.send(km);
            }
        } catch (Exception e) {
            logger.error("下载文件到本地并发送kafka失败,Method:{},error:{}", "sendToKafka", e);
        }
    }
}
