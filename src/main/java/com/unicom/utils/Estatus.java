package com.unicom.utils;

/**
 * Created by jinghui.zhang on 2016/1/8.
 */
public enum Estatus {
    BLACK_FAULT("F"),
    BLACK_SUCCESS("S"),
    BLACK_SUCCESSCCODE("200");

    private String estatus;

    private Estatus(String estatus) {
        this.estatus = estatus;
    }

    public String getEstatus() {
        return estatus;
    }

    @Override
    public String toString() {
        return this.estatus.toString();
    }

}
