package com.unicom.utils;

import java.util.concurrent.ArrayBlockingQueue;

/**
 * Created by bjut_zjh on 2016/3/23.
 */
public class GlobalQueue {
    public static String stormPath = "";
    public static ArrayBlockingQueue arrayBlockingQueue = new ArrayBlockingQueue(Integer.parseInt(ConfigUtil.getString("datacenter.unicom.queue.max.num")));

    public static String getStormPath() {
        return stormPath;
    }

    public static void setStormPath(String stormPath) {
        GlobalQueue.stormPath = stormPath;
    }
}
