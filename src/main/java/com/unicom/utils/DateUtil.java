package com.unicom.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Calendar;
import java.util.Date;

/**
 * Created by jinghui.zhang on 2016/1/12.
 */
public class DateUtil {
    private static Logger logger = LoggerFactory.getLogger(DateUtil.class);

    public static void toSleep() {
        try {
            Thread.sleep(1000 * 15);
        } catch (InterruptedException e) {
            logger.error("[Class:DateUtil][Method:toSleep][Message:{}][error:{}]",
                    "Thread.sleep error", e);
        }
    }

    public static String getDateStr() {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        calendar.add(Calendar.DAY_OF_MONTH, -1);
        Date date = calendar.getTime();
        String[] dateSplit = date.toLocaleString().split(" ")[0].split("-");
        String dateStr = dateSplit[0].substring(2) + (dateSplit[1].length() == 1 ? "0" + dateSplit[1] : dateSplit[1]) + dateSplit[2];
        return dateStr;
    }

    public static String getNowStr() {
        String date = new java.text.SimpleDateFormat("yyyyMMdd").format(new java.util.Date());
        return date;
    }

    public static void main(String[] args) {
        System.out.println(DateUtil.getNowStr());
    }
}
