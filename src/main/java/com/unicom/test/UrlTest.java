package com.unicom.test;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class UrlTest {
	public static void main(String[] args) {
		Pattern pattern = Pattern
				.compile("(http://|ftp://|https://|www|){0,1}[^\u4e00-\u9fa5\\s]*?\\.(com|net|cn|me|tw|fr)");
		// 空格结束
		Matcher matcher = pattern
				.matcher("http://api.haodou.com/index.php?appid=4&appkey=573bbd2fbd1a6bac082ff4727d952ba3&appsign=ae6bc3dabc14ab62ffcf5028af327097&channel=appstore&deviceid=0f607264fc6318a92b9e13c65db7cd3c%7C6D2D0B32-590A-4A63-811A-B141FBA03AD2%7C92919791-466C-4C8B-A9BD-6E847D482970&format=json&loguid=&method=Info.getInfo&nonce=1455292704&sessionid=1455292679&signmethod=md5&timestamp=1455292704&uuid=4b4e1a6816a0483dd12fecb344c04d14&v=2&vc=46&vn=v6.0.3");
		while (matcher.find()) {
			System.out.println(matcher.group(0));
		}
	}
}
